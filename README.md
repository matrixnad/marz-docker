# Requirements

```
Docker CLI
docker-compose
```

# Configuration

1. cd into site directory
2. move .env.example to .env

```
mv .env.example .env
```


# Run containers

To deploy the containers please use:

```
docker-compose up
```
# View app

To view the live app please go to http://localhost/

# Testing

- GET resource

To test the GET command, please use http://localhost/resource/get

- POST resource

To test the POST command please send a post to http://localhost/resource/set

For example, using CURL:
```
curl -XPOST --data-urlencode 'value=2' 'http://localhost/resource/set'
```