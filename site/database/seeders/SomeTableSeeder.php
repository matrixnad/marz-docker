<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SomeTable;

class SomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // initally set our value to 0
        SomeTable::create([
            'value' => 0
        ]);
    }
}
