-- create the users for each database
CREATE USER 'laravel'@'%' IDENTIFIED BY 'laravel';
CREATE DATABASE `laravel`;
GRANT ALL PRIVILEGES ON `laravel`.* TO 'laravel'@'%';

FLUSH PRIVILEGES;
