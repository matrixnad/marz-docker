<?php

use Illuminate\Support\Facades\Route;
use App\Models\SomeTable;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/resource/get', function () {
    // our SQL query
    $item = DB::table('some_table')
                     ->select(DB::raw('count(*) as table_count'), 'value')
                     ->groupBy('value')
                     ->first();
    return view('resource_get', compact('item'));
});
Route::post('/resource/set', function (Request $request) {
    $value = $request->get("value");
    // set our value
    $row = SomeTable::all()[0];
    $row->update([
        'value' => $value
    ]);
    return response("Value set");
});

