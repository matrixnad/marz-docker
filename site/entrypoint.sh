#!/bin/bash
echo "Wait for MySQL"
sleep 10

php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan schedule:work &
php-fpm
